<?php
namespace App\City;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class City extends DB
{
    public $id;
    public $name;
    public $city;

    public function __construct(){

        parent::__construct();
    }
    public function index(){
        echo "I'm inside the index of City Class";
    }
    public function setData($data=NULL){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists('city',$data)){
            $this->city=$data['city'];
        }
    }
    public function store(){
        $arrData = array($this->name,$this->city);
        $sql="insert into city(name, city) VALUES (?,?)";

        $STH= $this->DBH->prepare($sql); //create a object
        $result= $STH->execute($arrData);
        if($result)
            Message::setMessage("Success! Data has been inserted successfully!");
        else
            Message::setMessage("Fail! Data has not been inserted successfully");

        Utility::redirect('create.php');

    }

}
