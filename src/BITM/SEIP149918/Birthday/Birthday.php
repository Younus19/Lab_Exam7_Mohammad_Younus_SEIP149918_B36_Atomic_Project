<?php
namespace App\Birthday;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class Birthday extends DB
{
    public $id;
    public $name;
    public $birthday;

    public function __construct(){

        parent::__construct();
    }
    public function index(){
        echo "I'm inside the index of Birthday Class";
    }
    public function setData($data=NULL){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists('birthday',$data)){
            $this->birthday=$data['birthday'];
        }
    }
    public function store(){
        $arrData = array($this->name,$this->birthday);
        $sql="insert into birthday(name, birthday) VALUES (?,?)";

        $STH= $this->DBH->prepare($sql); //create a object
        $result= $STH->execute($arrData);
        if($result)
            Message::setMessage("Success! Data has been inserted successfully!");
        else
            Message::setMessage("Fail! Data has not been inserted successfully");

        Utility::redirect('create.php');

    }


}
